export const debounce = (fn, wait = 200) => {
    let timeout;

    return (...args) => {
        clearTimeout(timeout);
        timeout = setTimeout(fn, wait, ...args);
    };
};