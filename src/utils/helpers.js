export const formatDate = (query) => {
    const options = {
        year: 'numeric', month: 'numeric', day: 'numeric',
        hour: 'numeric', minute: 'numeric', second: 'numeric',
        hour12: false
    };

    const date = new Date(query);

    return new Intl.DateTimeFormat('en',  options).format(date);
}