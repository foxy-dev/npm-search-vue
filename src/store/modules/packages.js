import { search } from '@/api/api.algolia';
import { formatDate } from '@/utils/helpers';

const state = {
    packages: [],
    query: '',
    page: 1,
    limit: 10,
    totalPage: 0,
    loading: true,
    error: false,
};

const mutations = {
    PACKAGES_LIST: (state, packages) => {
        state.packages = packages;
    },

    PACKAGES_LIST_QUERY: (state, query) => {
        state.query = query;
    },

    PACKAGES_LIST_LOAD: (state) => {
        state.loading = true;
    },

    PACKAGES_LIST_SUCCESS: (state) => {
        state.loading = false;
        state.error = false;
    },

    PACKAGES_LIST_ERROR: (state) => {
        state.loading = false;
        state.error = true;
    },

    PACKAGES_LIST_PAGE: (state, page = 1) => {
        state.page = page;
    },

    PACKAGES_LIST_TOTAL_PAGE: (state, total = 0) => {
        state.totalPage = total;
    },
};

const actions = {
    async getPackages ({ state, commit }, params = {}) {
        commit('PACKAGES_LIST_LOAD');

        // try {
            const { page, query } = params;
            const result = await search(query || '', page || state.page, state.limit);

            const packages = result.hits.map((item) => ({
                ...item,
                deprecated: typeof item.deprecated === 'string' ? false : item.deprecated,
                created: formatDate(item.created),
                modified: formatDate(item.modified),
                versions: Object.keys(item.versions).reduce((all, current) => ({
                    ...all,
                    [current]: formatDate(item.versions[current]),
                }), {}),
            }));

            console.log(packages)
            commit('PACKAGES_LIST', packages);
            commit('PACKAGES_LIST_TOTAL_PAGE', result.nbPages);
            commit('PACKAGES_LIST_PAGE', result.page);
            commit('PACKAGES_LIST_QUERY', result.query);
            commit('PACKAGES_LIST_SUCCESS');
        // } catch {
        //     commit('PACKAGES_LIST_ERROR');
        // }
    }
};

const getters = {
    packagesList: (state) => state.packages,

    isLoading: (state) => state.loading,

    isError: (state) => state.error,

    packagesPagination: (state) => {
        return {
            total: state.totalPage,
            current: state.page,
        }
    },
};

export default {
    namespaced: true,
    state,
    mutations,
    getters,
    actions,
};
