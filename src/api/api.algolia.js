import algoliasearch from 'algoliasearch';

const API_KEY = 'f54e21fa3a2a0160595bb058179bfb1e';
const API_ID = 'OFCNCOG2CU';

const client = algoliasearch(API_ID, API_KEY);
const index = client.initIndex('npm-search');

export const search = (query, page = 0, hitsPerPage = 10) => {
    const options = {
        page,
        hitsPerPage,
        attributesToHighlight: [],
        attributesToRetrieve: [
            'deprecated',
            'description',
            'githubRepo',
            'homepage',
            'keywords',
            'license',
            'name',
            'owner',
            'version',
            'popular',
            'downloadsLast30Days',
            'downloadsRatio',
            'repository',
            'created',
            'modified',
            'readme',
            'changelogFilename',
            'homepage',
            'versions',
            'originalAuthor',
        ],
        analyticsTags: [ 'jsdelivr' ],
    };

    return index.search(query, options).then((response) => {
        response.hits.sort((a, b) => a.name === query ? -1 : b.name === query);

        return response;
    });
};